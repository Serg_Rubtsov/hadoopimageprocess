package org.orion;

import org.junit.Test;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;

import static org.junit.Assert.*;

/**
 * Created by sergei.rubtcov on 6/21/2017.
 */
public class EncryptionMapperTest {

    @Test
    public void png() throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();
        File f = new File(classLoader.getResource("img.bmp").getFile());
        FileInputStream fis = new FileInputStream(f);
        BufferedImage bmp = ImageIO.read(fis); // reading the image file
        BufferedImage png = Utils.getPngBufferedImage(bmp);
        assertNotNull(png);
        assertEquals(1000, png.getHeight());
    }

}