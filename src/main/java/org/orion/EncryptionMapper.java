package org.orion;

import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapreduce.Mapper;
import org.orion.mipr.ImageWritable;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.Map;

public class EncryptionMapper extends Mapper<NullWritable, ImageWritable, NullWritable, ImageWritable> {

    private Map<Integer, LogisticObjectUpdate[][]> mapVal = new LinkedHashMap<>();
    private Map<Integer, LogisticObjectUpdate[][]> mapSampleVal = new LinkedHashMap<>();

    private int count1 = 0;
    private int count2 = 0;
    private int rows, cols;

    private String inputFileName;
    private String splitDecryptedFileName;
    private String splitEncryptedFileName;
    private String miniImageJpgFileName;
    private String encryptedImageFileName;
    private String decryptedImageFileName;
    private Context context;
    private String format;

    @Override
    protected void map(NullWritable key, ImageWritable value, Context context) throws IOException, InterruptedException {
        try {
            this.context = context;
            this.inputFileName = value.getFileName();
            this.format = value.getFormat();
            this.splitDecryptedFileName = "split_img_Decrypted" + inputFileName;
            this.splitEncryptedFileName = "split_img_Encrypted" + inputFileName;
            this.miniImageJpgFileName = "split_img" + inputFileName;
            this.encryptedImageFileName = "encrypted" + inputFileName;
            this.decryptedImageFileName = "decrypted" + inputFileName;
            int index = 0;
            BufferedImage imgFinalEncrypt[] = new BufferedImage[2000];
            BufferedImage imgFinalDecrypt[] = new BufferedImage[2000];
            BufferedImage imageTemp = value.getImage(); // reading the image file
            int orgImgWidth = imageTemp.getWidth(); // determines image size
            // width
            int orgImgHeight = imageTemp.getHeight();
            rows = (int) Math.ceil(orgImgWidth / 1000.0);
            cols = (int) Math.ceil(orgImgHeight / 1000.0);
            int chunks = rows * cols;
            if (imageTemp.getWidth() >= 1000 || imageTemp.getHeight() >= 1000) {
                // Split the big image file into smaller ones
                BufferedImage[] imgs = splitFile(imageTemp, rows, cols, chunks);
                for (int i = 0; i < imgs.length; i++) {
                    // Encrypt the split image with secret key
                    BufferedImage image = encryptImage(imgs[i]);
                    imgFinalEncrypt[index++] = image;
                }
                // merge all the encrypted split images to one
                merge(imgFinalEncrypt);
                // Decrypt all split images one by one
                index = 0;
                count1 = 0;
                count2 = 0;
                for (int i = 0; i < imgs.length; i++) {
                    BufferedImage image = decryptImage(imgFinalEncrypt[i]);
                    imgFinalDecrypt[index++] = image;
                }
                // merge all the decrypted split images to one
                mergeFinal(imgFinalDecrypt);
            } else {
                processFile(imageTemp);
            }
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    private BufferedImage decryptImage(BufferedImage originalimg) throws Exception {
        int width = originalimg.getWidth();
        int height = originalimg.getHeight();
        LogisticObjectUpdate xorObject[][] = mapVal.get(count2++);
        LogisticObjectUpdate sampleObject[][] = mapSampleVal.get(count1++);
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                int decryptKey0 = (int) xorObject[i][j].A;
                int decryptKey1 = (int) (sampleObject[i][j].A * 255);
                Color color = new Color(originalimg.getRGB(i, j));
                Color newColor = new Color((color.getRed() ^ decryptKey0 ^ decryptKey1),
                        (color.getGreen() ^ decryptKey0 ^ decryptKey1),
                        (color.getBlue() ^ decryptKey0 ^ decryptKey1));
                originalimg.setRGB(i, j, newColor.getRGB());
            }
        }
        int[][] originalImgMatrix = convertTo2DUsingGetRGB(originalimg);
        int[][] originalImgMatrixFinal = new int[width][height];
        //Arrange original matrix based on secret key matrix positions
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                int posX = sampleObject[i][j].B;
                int posY = sampleObject[i][j].C;
                originalImgMatrixFinal[posX][posY] = originalImgMatrix[i][j];
            }
        }
        originalimg = createImage(originalImgMatrixFinal, originalimg);
        ImageWritable biw = new ImageWritable(originalimg, splitDecryptedFileName + count1, format);
        context.write(NullWritable.get(), biw);
        return originalimg;
    }

    private BufferedImage encryptImage(BufferedImage originalImg) throws Exception {
        double logisticXn = 0.675;
        double logisticR = 3.99;
        int width = originalImg.getWidth();
        int height = originalImg.getHeight();
        LogisticObjectUpdate sampleObject[][] = new LogisticObjectUpdate[width][height];
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                sampleObject[i][j] = new LogisticObjectUpdate();
                sampleObject[i][j].A = logistic(logisticXn, logisticR);
                sampleObject[i][j].B = i;
                sampleObject[i][j].C = j;
                logisticXn = sampleObject[i][j].A;
            }
        }
        // sort the secret key matrix
        Arrays.sort(sampleObject,
                new Comparator<LogisticObjectUpdate[]>() {
                    public int compare(LogisticObjectUpdate[] a, LogisticObjectUpdate[] b) {
                        Double val1 = a[0].getA();
                        Double val2 = b[0].getA();
                        return val1.compareTo(val2);
                    }
                });
        mapSampleVal.put(count1++, sampleObject);
        int newSize = (width * height) / 256;
        int xorLogisticObjectCounter = 0;
        int xor = 0;
        LogisticObjectUpdate xorObject[][] = new LogisticObjectUpdate[width][height];
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                xorObject[i][j] = new LogisticObjectUpdate();
                if (xorLogisticObjectCounter < newSize) {
                    xorObject[i][j] = new LogisticObjectUpdate();
                    xorObject[i][j].A = xor;
                    xorLogisticObjectCounter++;
                } else {
                    xorLogisticObjectCounter = 0;
                    xor++;
                }
            }
        }
        mapVal.put(count2++, xorObject);
        int[][] originalImgMatrix = convertTo2DUsingGetRGB(originalImg);
        int[][] originalImgMatrixFinal = new int[width][height];
        //Arrange original matrix based on secret key matrix positions
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                int posX = sampleObject[i][j].B;
                int posY = sampleObject[i][j].C;
                originalImgMatrixFinal[i][j] = originalImgMatrix[posX][posY];
            }
        }
        originalImg = createImage(originalImgMatrixFinal, originalImg);
        //XOR the image with the created matrix of keys (0-255)
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                int encryptKey0 = (int) (sampleObject[i][j].A * 255);
                int encryptKey1 = (int) (xorObject[i][j].A);
                Color color = new Color(originalImg.getRGB(i, j));
                Color newColor = new Color((color.getRed() ^ encryptKey0 ^ encryptKey1),
                        (color.getGreen() ^ encryptKey0 ^ encryptKey1),
                        (color.getBlue() ^ encryptKey0 ^ encryptKey1));
                originalImg.setRGB(i, j, newColor.getRGB());
            }
        }
        //write image
        ImageWritable biw = new ImageWritable(originalImg, splitEncryptedFileName + count1, format);
        context.write(NullWritable.get(), biw);
        return originalImg;
    }

    //Generate logistic numbers
    private static double logistic(double xn, double r) {
        return r * xn * (1.0 - xn);
    }

    private BufferedImage[] splitFile(BufferedImage image, int rows, int cols,
                                      int chunks) throws Exception {
        int chunkWidth = image.getWidth() / cols; // determines the chunk width
        int chunkHeight = image.getHeight() / rows;
        int count = 0;
        BufferedImage imgs[] = new BufferedImage[chunks]; // Image array to hold
        // image chunks
        for (int x = 0; x < rows; x++) {
            for (int y = 0; y < cols; y++) {
                // Initialize the image array with image chunks
                imgs[count] = new BufferedImage(chunkWidth, chunkHeight,
                        BufferedImage.TYPE_INT_ARGB);
                // draws the image chunk
                Graphics2D gr = imgs[count++].createGraphics();
                gr.drawImage(image, 0, 0, chunkWidth, chunkHeight, chunkWidth
                                * y, chunkHeight * x, chunkWidth * y + chunkWidth,
                        chunkHeight * x + chunkHeight, null);
                gr.dispose();
            }
        }
        image.flush();
        //Splitting done
        return imgs;
    }

    private void merge(BufferedImage[] images) throws Exception {
        int type = images[0].getType();
        int chunkWidth = images[0].getWidth();
        int chunkHeight = images[0].getHeight();
        // Initializing the final image
        BufferedImage finalImg = new BufferedImage(chunkWidth * cols,
                chunkHeight * rows, type);
        int num = 0;
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                finalImg.createGraphics().drawImage(images[num],
                        chunkWidth * j, chunkHeight * i, null);
                num++;
            }
        }
        //Image concatenated merge called
        ImageWritable biw = new ImageWritable(finalImg, encryptedImageFileName, format);
        context.write(NullWritable.get(), biw);
    }

    private void mergeFinal(BufferedImage[] images) throws Exception {
        int type = images[0].getType();
        int chunkWidth = images[0].getWidth();
        int chunkHeight = images[0].getHeight();
        // Initializing the final image
        BufferedImage finalImg = new BufferedImage(chunkWidth * cols,
                chunkHeight * rows, type);
        int num = 0;
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                finalImg.createGraphics().drawImage(images[num],
                        chunkWidth * j, chunkHeight * i, null);
                num++;
            }
        }
        //Image concatenated merge called
        ImageWritable biw = new ImageWritable(finalImg, decryptedImageFileName, format);
        context.write(NullWritable.get(), biw);
    }

    private void processFile(BufferedImage image) throws Exception {
        BufferedImage imageEncrypted = encryptImage(image);
        ImageWritable biw = new ImageWritable(imageEncrypted, encryptedImageFileName, format);
        context.write(NullWritable.get(), biw);
        count1 = 0;
        count2 = 0;
        BufferedImage imageDecrypted = decryptImage(imageEncrypted);
        //Image concatenated
        biw = new ImageWritable(imageDecrypted, decryptedImageFileName, format);
        context.write(NullWritable.get(), biw);
    }

    private static int[][] convertTo2DUsingGetRGB(BufferedImage image) {
        int width = image.getWidth();
        int height = image.getHeight();
        int[][] result = new int[width][height];
        for (int row = 0; row < width; row++) {
            for (int col = 0; col < height; col++) {
                result[row][col] = image.getRGB(row, col);
            }
        }
        return result;
    }

    private static BufferedImage createImage(int[][] pixelData, BufferedImage image) {
        //create Image from this PixelArray
        BufferedImage result = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_INT_RGB);
        int width = image.getWidth();
        int height = image.getHeight();
        for (int n = 0; n < width; n++) {
            for (int m = 0; m < height; m++) {
                result.setRGB(n, m, pixelData[n][m]);
            }
        }
        return result;
    }

}
